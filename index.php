<?php require "./code.php" ?>

<!DOCTYPE html>

<html>
<head>
	<title>Array Manipulation</title>
</head>
<body>
	<h2>Create Product</h2>
    <?php createProduct($products,"product1", 500); ?>
	<?php createProduct($products,"product2", 900); ?>
	<?php createProduct($products,"product3", 1100); ?>
    <p>
        <?php print_r($products); ?>
    </p>

    <ul>
        <?php printProducts($products); ?>
    </ul>

    <span>
        <?php countProducts($products); ?>
    </span>
    <span>
        <?php deleteProduct($products); ?>
    </span>
     <p>
        <?php print_r($products); ?>
    </p>



</body>
</html>
